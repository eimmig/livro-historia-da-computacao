# Calculadoras Mecânicas

   Desde dos primórdios da existência humana, a matemática é um elemento imprescindível para a evolução estando presente na maioria dos problemas. Sendo eles na construção de edifícios, medidas, economia, brincadeiras e educação. 

   Entretanto, muitas vezes, ao se deparar com problemas matemáticos mais complexos, temos tendência ao erro que a partir disso pode gerar uma cadencia de problemas maiores. Pensando nisso, criaram-se meios de automatizar o sistema de cálculo com o objetivo de diminuir as falhas e ainda economizar tempo, conhecidas como calculadoras. 
   
Dentre esses projetos se destacam: 

- O ábaco foi a primeira calculadora da história. Criado pelos chineses no século 6a.C, esse instrumento dispunha de fios paralelos e arruelas deslizantes, capazes de realizar contas de adição e subtração. 
[Figura1](imagens/Calculadora-abaco.jpg)

- Em 1642 o matemático francês Blaise Pascal, inventou uma máquina automática de cálculos para agilizar o trabalho do seu pai. Porém sua funcionalidade limitava-se às operações de adição e subtração.
[Figura2](imagens/Calculadora_de_Pascal.jpg)

- Em 1671, o filósofo e matemático alemão Gottfried Wilhelm Von Leibniz desenvolveu um mecanismo capaz de realizar as quatro operações, adição, subtração, multiplicação e divisão: a chamada “roda graduada”.
[Figura3](imagens/graduada.jpg)

-  Em 1823, Charles Baddge, criou a Máquina Diferencial de Babbage Permite calcular tabelas de funções (logaritmos, funções trigonométricas, etc.) sem a intervenção de um operador humano.
[Figura4](imagens/ProjetoBabbage.jpg)

- Em 1947, o austríaco Curt Herzstark desenvolveu o projeto da primeira calculadora mecânica, reduzida ao tamanho de um copo.
 [Figura5](imagens/Calculadora_copo.jpg)




# Referências: 

1. BRAGA, Benedito et al. Introdução à engenharia ambiental: o desafio do desenvolvimento sustentável. Pearson Prentice Hall, 2005.

1. FONSECA FILHO, Cléuzio. História da computação: O Caminho do Pensamento e da Tecnologia. EDIPUCRS, 2007.

1. VALLIM, Marcos Banheti Rabello et al. Em direção à melhoria do ensino na área tecnológica: a experiência de uma disciplina de introdução à engenharia de controle e automação. 2000.
