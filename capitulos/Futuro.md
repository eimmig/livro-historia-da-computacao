# Futuro:

A Inteligência Artificial é definida, desde seus primórdios, como a ciência e engenharia de fazer máquinas inteligentes, termo proposto por John McCarthy em 1956 na conferência de Dartmouth, nos Estados Unidos.

Inteligência artificial é a inteligência similar à humana exibida por sistemas de software, além de também ser um campo de estudo acadêmico.
Com o passar dos tempos, fica evidente que o avanço da Inteligência Artificial (IA), proporciona diversos benefícios para a humanidade, e muitos riscos com ela.
Surgem duvidas e mais duvidas de como capacitar a IA garantindo a ética e a responsabilidade social no trabalho que vem a ser executado ou ate mesmo, qual o conteúdo necessário para um curso e para uma formação em Inteligência Artificial.
Diante desses questionamentos, fica claro que ainda não estamos aptos a trabalhar com tal tecnolgoia, e que ainda é necessário muita pesquisa e compromentimento com a tecnologia para que possa desenvolver com maestria as aplicações.



# Referênicas:

1. CARVALHO, André; GIMENES, Itana. O Futuro da Educação em IA: Pública, Privada ou Institucional?. Computação Brasil, n. 43, p. 31-34, 2020. 
