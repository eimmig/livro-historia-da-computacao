# Evolução dos Computadores Pessoais e sua Interconexão - Primeira Revolução

A evolução dos computadores acompanhou a evolução da sociedade durante os séculos século XX e XXI. A palavra Computador vem do verbo Computar, que por sua vez deriva do Latim “COMPUTARE”, significando Calcular. Entretanto, a história do computador não teve início apenas na modernidade.
Por isso, podemos pensar que a criação de computadores começa na idade antiga, já que para contar, muitos instrumentos foram criados desde a antiguidade.

O computador, como conhecemos hoje, passou por diversas transformações e foi se aperfeiçoando ao longo do tempo para se transformar no que é hoje, acompanhando o avanço das principais áreas que acompanham a Computação: Matemática, engenharia, eletrônica. Por esse motivo, não existe somente um inventor.

A história da computação está dividida em quatro períodos. Sendo a primeira nomeada de Primeira Geração. Os computadores dessa geração funcionavam por meio de circuitos e válvulas eletrônicas. Possuíam o uso restrito, além de serem imensos e consumirem muita energia.
Um exemplo é o ENIAC (Eletronic Numerical Integrator and Computer) que consumia cerca de 200 quilowatts e possuía 19.000 válvulas, comparando com o de hoje em dia que consome cerca de 130 quilowatts por mês e dificilmente se encontra um eletro-eletrônico usando válvulas. Desapareceram, já que foram evoluíndo das válvulas para o transístor e depois para o circuito integrado.


# Referências

https://www.todamateria.com.br/historia-e-evolucao-dos-computadores/

https://mundoeducacao.uol.com.br/informatica/evolucao-dos-computadores.htm

https://www.hardware.com.br/livros/hardware/evolucao-dos-computadores-pessoais.html
