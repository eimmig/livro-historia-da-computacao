# Primeiros Computadores


Primeiramente a pessoa que começou o projeto do primeiro computador mecanico foi Charles Babbage. Ele foi foi um cientista, matemático e professor da Universidade de Cambridge.Em 1821, Charles Babbage começou a automatizar a produção de cálculos matemáticos. A ideia era de criar uma máquina que conseguisse acabar de uma vez por todas com os erros que apareciam constantemente em tabelas de logaritmos, o resultado de tanta pesquisa e esforço foi o projeto da Máquina Diferencial, na qual foi a base para os futuros computadores. Diferentemente de calculadoras que surgiram antes, o invento foi desenvolvido para calcular uma série de valores numéricos e imprimir os resultados automaticamente.
Babbage ainda desenhou um segundo modelo, mas as ferramentas e componentes necessários para tirar as invenções do papel estavam muito à frente da época.

  Charles Babbage, considerado o pai do computador atual, desenvolveu um projeto que apresentava desvantagens; uma delas era o fato de que o seu computador deveria ser mecânico, e a outra era a precariedade da engenharia da época. Apesar dos problemas, Charles Babbage construiu um aparelho que impressionou o governo inglês e todos que o viam.

  [Projeto Babbage](imagens/ProjetoBabbage.jpg)

  O primeiro computador eletromecânico foi inventado em 1936 por Konrad Zuze.
  
  
  [Primeiro Computador](imagens/PrimeiroComputador.jpg)
  
  
  Ja o primeiro computador digital eletrônico de grande escala: Foi o ENIAC (Electrical Numerical Integrator and Calculator). Este computador foi criado em de 1946 pelos cientistas norte-americanos John Presper Eckert e John W. Mauchly.


 # Referencias: 
 Tec-Mundo: https://www.tecmundo.com.br/historia/16641-charles-babbage-um-cientista-muito-alem-de-seu-tempo.htm;
 
 Google Academico: https://sites.google.com/site/historiasobreossitesdebusca/Historia-da-tecnologia/historia-do-primeiro-computador.




  





