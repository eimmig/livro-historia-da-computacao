# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão listados abaixo.

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/PrimeirosComputadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/EvoluçãoComputadoresPessoaisInterconexão.md)
1. [Computação Móvel](capitulos/ComputacaoMovel.md)
1. [Futuro](capitulos/Futuro.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9867497/avatar.png?width=400)  | Eduardo Mateus Immig | eimmig | [eimmig@alunos.utfpr.edu.br](mailto:eimmig@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9867357/avatar.png?width=400)  | Danilo Pietrobon Neto | DaniloNeto1 | [danilopietrobon@alunos.utfpr.edu.br](mailto:danilopietrobon@alunos.utfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9983024/avatar.png?width=400) | Gabriel Henrique Silvestre De Lima | gabriellima.2002 | [gabriellima.2002@alunos.utfpr.edu.br](mailto:gabriellima.2002@alunos.utfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9982821/avatar.png?width=400)  | Tiago Aureliano Lança Rodrigues | tiagorodriguez | [tiagorodriguez@alunos.utfpr.edu.br](mailto:tiagorodriguez@alunos.utfpr.edu.br) |
